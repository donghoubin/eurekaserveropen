package com.open.eurekaServerOpen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class EurekaServerOpenApplication {

	public static void main(String[] args) {
		SpringApplication.run(EurekaServerOpenApplication.class, args);
	}

}
